package test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import table.Table;

import static com.codeborne.selenide.Selenide.*;

public class TableTest {

    @BeforeClass
    public void setUp() {
        Configuration.timeout = 5000;
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));

        open("https://www.w3schools.com/html/");
    }

    @AfterClass
    public void tearDown() {
        closeWebDriver();
    }

    @Test
    public void tableTest() {
        $x("//a[contains(text(),'HTML Tables')]").click();

        WebElement tableElement = $x("//table[@id='customers']");

        Table table1 = new Table(tableElement);

        System.out.println("Количество строк " + table1.getRows().size());

        table1.getHeaders().forEach(elem->
                System.out.println("Заголовок " + elem.getText()));

        System.out.printf("Распечатать название по строке %d и столбцу %d - %s"
                , 2, 3, table1.getNameFromCell(2, 3));
    }
}
