package test;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.AutorizationPageSteps;

import static com.codeborne.selenide.Selenide.*;

@Feature("Авторизация")
public class AutorizationTest extends AutorizationPageSteps {

    @BeforeClass
    public void setUp() {
        Configuration.timeout = 5000;
        Configuration.browser = "chrome";
        Configuration.startMaximized = true;

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));

        open("https://www.saucedemo.com/");
    }

    @Test(description = "Авторизация стандартным пользователем")
    @Story("TC-1212")
    public void test1() {
        //шаг 1 открываем сайт - ОР сайт открыт
        verifySite();
        //шаг 2 вводим логин - ОР логин введен
        writeLoginAndVerify("standard_user");
        //шаг 3 вводим пароль - ОР пароль введен
        writePasswordAndVerify("secret_sauce");
        //шаг 4 кликаем на кнопку логин - ОР открывается главная страница
        clickButtonAndVerifySite();
    }

    @AfterClass
    public void tearDown() {
        closeWebDriver();
    }
}
