package steps;

import com.codeborne.selenide.Condition;
import elements.AutorizationPageForm;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.webdriver;
import static com.codeborne.selenide.WebDriverConditions.url;

public class AutorizationPageSteps {

    AutorizationPageForm autorizationPageForm = new AutorizationPageForm();

    @Step("открываем сайт - ОР сайт открыт")
    public void verifySite() {
        autorizationPageForm.logotype.shouldBe(Condition.visible);
        autorizationPageForm.logotype.shouldBe(checked);
        webdriver().shouldHave(url("https://www.saucedemo.com/"));
    }

    @Step("вводим логин - ОР логин введен")
    public void writeLoginAndVerify(String login) {
        autorizationPageForm.login.shouldBe(visible);
        autorizationPageForm.login.setValue(login);
        autorizationPageForm.login.shouldHave(value(login));
    }

    @Step("вводим пароль - ОР пароль введен")
    public void writePasswordAndVerify(String password) {
        autorizationPageForm.password.shouldBe(visible);
        autorizationPageForm.password.setValue(password);
        autorizationPageForm.password.shouldHave(value(password));
    }

    @Step("кликаем на кнопку логин - ОР открывается главная страница")
    public void clickButtonAndVerifySite() {
        autorizationPageForm.buttonLogin.shouldBe(visible).click();
        autorizationPageForm.header.shouldBe(appear);
        autorizationPageForm.header.shouldHave(text("Products"));
        webdriver().shouldHave(url("https://www.saucedemo.com/invento.html"));
    }
}
