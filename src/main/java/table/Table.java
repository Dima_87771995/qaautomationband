package table;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class Table {

    private WebElement tableElement;

    public Table(WebElement tableElement) {
        this.tableElement = tableElement;
    }

    public List<WebElement> getRows() {
        List<WebElement> rows = tableElement.findElements(By.xpath(".//tr"));
        rows.remove(0);
        return rows;
    }

    public List<WebElement> getHeaders() {
        WebElement headersRow = tableElement.findElement(By.xpath(".//tr[1]"));
        List<WebElement> headerColumns = headersRow.findElements(By.xpath(".//th"));
        return headerColumns;
    }

    //разобьем строки на колонки(столбцы)
    public List<List<WebElement>> getRowsWithColumns() {
        // Получим все строки
        List<WebElement> rows = getRows();
        List<List<WebElement>> rowsWithColumns = new ArrayList<List<WebElement>>();
        // Пробегаем по строкам и каждую строку разбиваем на столбцы
        // В каждой строке выполняем findElements, чтобы найти наши столбцы
        for (WebElement row: rows) {
            List<WebElement> rowWithColumns = row.findElements(By.xpath(".//td"));
            rowsWithColumns.add(rowWithColumns);
            // каждую разбитую строку на столбцы мы добавляем в новый список, таким образом формируем список строк разбитых на столбцы
        }
        // После создания списка из строк разбитых на столбцы мы его вернем
        return rowsWithColumns;
    }

    // Далее напишем метод который позволит работать со списком getRowsWithColumns
    // получение значения из ячейки по номеру строки и столбца
    public String getNameFromCell(int rowNumber, int columnNumber) {
        List<List<WebElement>> rowsWithColumns = getRowsWithColumns();
        // Cначала получим нужную строку
        WebElement cell = rowsWithColumns.get(rowNumber - 1).get(columnNumber - 1);
        return cell.getText();
    }
}
